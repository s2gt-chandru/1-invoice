﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneInvoice.Models
{
    public class Invoice
    {
        public string invoiceId { get; set; }
        public DateTime  invoicedate { get; set; }
        public string from { get; set; }
        public int invoiceamount { get; set; }
        public string status { get; set; }
    }
}