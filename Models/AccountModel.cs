﻿using Newtonsoft.Json;
using RSSEntity.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace OneInvoice.Models
{
    public class AccountModel
    {
        public static User IsUserExists(string userName, string password)
        {
            HttpResponseMessage response = null;
            User loggedInUser = null;
            HttpClient client = new HttpClient();

            //  userName = (!string.IsNullOrEmpty(userName)) ? encodeString(userName) : userName;

            if (!string.IsNullOrEmpty(userName))
            {
                userName = Utils.encodeString(userName);
            }

            if (!string.IsNullOrEmpty(password))
            {
                password = Utils.encodeString(password);
            }

            byte[] credentialBytes = Encoding.ASCII.GetBytes(Utils.credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic"
                , Convert.ToBase64String(credentialBytes));

            try
            {
                using (client)
                {
                    response = client.GetAsync(Utils.baseAddress + "Account/ValidateUser?UserName=" + userName + "&Password=" + password).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        loggedInUser = response.Content.ReadAsAsync<User>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                //todo: silent catch
                Utils.Log(ex);
            }

            return loggedInUser;
        }

        public static int ModifyUserTerms(int userId)
        {
            HttpResponseMessage response = null;
            string jsonRequest = JsonConvert.SerializeObject(userId);
            jsonRequest = Utils.encodeString(jsonRequest);
            int successCode = -1;
            HttpClient client = new HttpClient();

            byte[] credentialBytes = Encoding.ASCII.GetBytes(Utils.credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic"
                , Convert.ToBase64String(credentialBytes));

            try
            {
                using (client)
                {
                    response = client.PostAsync(Utils.baseAddress + "Account/ModifyUserTerms" + jsonRequest, null).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        successCode = response.Content.ReadAsAsync<int>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                //todo: silent catch
                Utils.Log(ex);
            }

            return successCode;
        }

    }
}