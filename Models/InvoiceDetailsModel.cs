﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RSSCommon.Models;
using RSSEntity.Admin;
using RSSEntity.Case;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace OneInvoice.Models
{
    public class InvoiceDetailsModel
    {
        public static CaseDetails GetCaseDetails(string rsId)
        {
            HttpResponseMessage response = null;
            CaseDetails caseDetails = null;
            HttpClient client = new HttpClient();

            byte[] credentialBytes = Encoding.ASCII.GetBytes(Utils.credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic"
                , Convert.ToBase64String(credentialBytes));

            try
            {
                using (client)
                {
                    response = client.GetAsync("https://testapi.readysetsurgical.com/api/User/GetCaseDetails?RSID=" + rsId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        caseDetails = response.Content.ReadAsAsync<CaseDetails>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                //todo: silent catch
                Utils.Log(ex);
            }

            return caseDetails;
        }

        public static string CreateCase(Case casemodel)
        {
            HttpResponseMessage response = null;
            string jsonRequest = JsonConvert.SerializeObject(casemodel);
            jsonRequest = Utils.encodeString(jsonRequest);
            string rsId = string.Empty;
            HttpClient client = new HttpClient();

            byte[] credentialBytes = Encoding.ASCII.GetBytes(Utils.credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic"
                , Convert.ToBase64String(credentialBytes));

            try
            {
                using (client)
                {
                    response = client.PostAsync("https://testapi.readysetsurgical.com/api/User/CreateCase?request=" + jsonRequest, null).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        rsId = response.Content.ReadAsAsync<string>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                //todo: silent catch
                Utils.Log(ex);
            }

            return rsId;
        }

        public static string AddInvoice(Invoice invoice)
        {
            string invoiceId = string.Empty;
            string sql = string.Format(@"INSERT INTO `rss`.`invoices` 
                            (`invoiceId`,
	                        `invoicedate`,
	                        `from`,
	                        `invoiceamount`,
	                        `status`
	                        )

                            VALUES
                            ('{0}',
                             @invoicedate,
                            '{1}',
                            '{2}',
                            '{3}'
                            );SELECT LAST_INSERT_ID()", invoice.invoiceId, invoice.from, invoice.invoiceamount, invoice.status);
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();

                using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@invoicedate", DateTime.Now);
                    //cmd.Parameters.AddWithValue("@passwd", password);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows && reader.Read())
                    {
                        invoiceId = reader.GetString(0);
                    }
                }

                connection.Close();
            }

            return invoiceId;
        }

        private static MySqlConnection GetConnection()
        {
            string Connection = ConfigurationManager.ConnectionStrings["strRSSConnection"].ConnectionString;

            return new MySqlConnection(Connection);
        }
    }
}