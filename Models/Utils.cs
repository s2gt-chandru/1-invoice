﻿using log4net;
using RSSEntity.Admin;
using System;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace OneInvoice.Models
{
    public class Utils
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string EncryptionKey = "ReadySetSurgical@2014";
        public static string baseAddress = ConfigurationManager.AppSettings["WEBAPIBaseAdress"];
        public static string ReportBaseAddress = ConfigurationManager.AppSettings["ReportAPIBaseAdress"];
        public static string credentials = ConfigurationManager.AppSettings["User_Token"].ToString() 
            + ":" + ConfigurationManager.AppSettings["Pass_Token"].ToString();


        //public static User IsUserExists(string userName, string password)
        //{
        //    HttpResponseMessage response = null;
        //    User loggedInUser = null;
        //    HttpClient client = new HttpClient();

        //  //  userName = (!string.IsNullOrEmpty(userName)) ? encodeString(userName) : userName;

        //    if (!string.IsNullOrEmpty(userName))
        //    {
        //        userName = encodeString(userName);
        //    }

        //    if (!string.IsNullOrEmpty(password))
        //    {
        //        password = encodeString(password);
        //    }

        //    byte[] credentialBytes = Encoding.ASCII.GetBytes(credentials);
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic"
        //        , Convert.ToBase64String(credentialBytes));

        //    try
        //    {
        //        using (client)
        //        {
        //            response = client.GetAsync(baseAddress + "ValidateUser?UserName=" + userName + "&Password=" + password).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                loggedInUser = response.Content.ReadAsAsync<User>().Result;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //todo: silent catch
        //        log.Error(ex.Message, ex);
        //    }

        //    return loggedInUser;
        //}

        internal static void Log(Exception ex)
        {
            log.Error(ex.Message, ex);
        }
        
        // password encrypt
        public static string Encrypt(string clearText)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        //username and password
        public static string encodeString(string content)
        {
            log.Info(" ## " + MethodBase.GetCurrentMethod().Name + " ## ");

            string encodedContent = "";
            try
            {
                byte[] encodedBytes = CompressString(content);
                encodedContent = HttpServerUtility.UrlTokenEncode(encodedBytes);
            }
            catch (Exception ex)
            {
                //todo: silent catch?
                log.Error(ex.Message, ex);
            }

            return encodedContent;
        }

        public static byte[] CompressString(String str)
        {
            using (MemoryStream output = new MemoryStream())
            {
                using (DeflateStream gzip = new DeflateStream(output, CompressionMode.Compress))
                {
                    using (StreamWriter writer = new StreamWriter(gzip, Encoding.UTF8))
                    {
                        writer.Write(str);
                    }
                }

                return output.ToArray();
            }
        }

        public static string GetRandomRSID(string str)
        {
            Random random = new Random();
            int randomrsid = random.Next(10000000, 99999999);
            string randomgeneratedrsid = str + randomrsid.ToString();
            return randomgeneratedrsid;
        }
    }
}