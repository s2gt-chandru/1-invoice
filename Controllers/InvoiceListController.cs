﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneInvoice.Controllers
{
    [Authorize]
    public class InvoiceListController : Controller
    {
        // GET: InvoiceList
        public ActionResult InvoiceList()
        {
            return View();
        }
    }
}