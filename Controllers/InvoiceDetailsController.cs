﻿using OneInvoice.Models;
using RSSCommon;
using RSSCommon.Models;
using RSSEntity.Admin;
using RSSEntity.Case;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneInvoice.Controllers
{
    [Authorize]
    public class InvoiceDetailsController : Controller
    {
        // GET: InvoiceDetails
       
        public ActionResult InvoiceDetails()
        {
            return View();
        }

        public ActionResult GetCaseDetails(string id)
        {
            CaseDetails caseDetails = InvoiceDetailsModel.GetCaseDetails(id);
            string hospital = caseDetails.cse.HospitalName.ToString();

            return Content(hospital);
        }

        [HttpGet]
        public ActionResult CreateCase(string RSID, string startTime, string endTime, string fromdate, string todate)
        {
            CaseModel casemodel = new CaseModel();
            User loggedInUser = (Session[SessionState.USERDETAIL] as User);

            //casemodel = getCaseModelWithoutRSID(casemodel, RSID, loggedInUser);
            return View(casemodel);
        }

        [HttpPost] //todo 
        public ActionResult Create(CaseModel casemodel, string stpartdate = "", string etpartdate = "", string fromdate = "", string todate = "")
        {
            
            User loggedInUser = (Session[SessionState.USERDETAIL] as User);
            Case objCase = new Case();
            string randomgeneratedrsid = Utils.GetRandomRSID("RS");

            casemodel.RSID = randomgeneratedrsid;
            objCase.RSID = casemodel.RSID;
            objCase.CaseName = casemodel.CaseName;
            objCase.ColorCode = casemodel.ColorCode;
            objCase.Description = casemodel.Description;
            objCase.CaseType = casemodel.CaseType;
            objCase.Hospital = casemodel.Hospital.ToString();
            objCase.HospitalName = casemodel.HospitalName;
            objCase.Notes = casemodel.Notes;

            string rsId = InvoiceDetailsModel.CreateCase(objCase);

            return RedirectToAction("CreateCase");
        }

        public ActionResult CreateInvoice(Invoice modal)
        {
            User loggedInUser = (Session[SessionState.USERDETAIL] as User);
            Invoice invoice = new Invoice();
            invoice.invoiceId = Utils.GetRandomRSID("IV");
            invoice.from = loggedInUser.UserId.ToString();
            invoice.invoiceamount = modal.invoiceamount;
            invoice.status = "waiting";
            string invoiceId = InvoiceDetailsModel.AddInvoice(invoice);
            
            return RedirectToAction("InvoiceList", "InvoiceList");
        }
    }
}