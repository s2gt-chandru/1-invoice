﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OneInvoice.Startup))]
namespace OneInvoice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
